﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

namespace ReadTextFile
{
    class Program
    {
        static void Main(string[] args)
        {
            // for (int i = 0; i < args.Length; i++)
            // {
            //     Console.WriteLine($"{i} - {args[i]}");
            // }
            // string path = @"E:\code\fdx\frt\nvrevents\ReadTextFile\devices.txt";
            List<string> lines = new List<string>(File.ReadLines(@args[0]));
            Console.WriteLine($"AppDomain.CurrentDomain.BaseDirectory : {AppDomain.CurrentDomain.BaseDirectory}");
            lines.ForEach(line => {
                string[] info = line.Split(" ", StringSplitOptions.RemoveEmptyEntries);
                Console.WriteLine($"> IP: {info[0]} | Port: {info[1]} | Username: {info[2]} | Password: {info[3]}");
            });
        }
    }
}
