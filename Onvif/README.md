# Onvif Plugin
This project is a plugin for shopeye-agent to connect to camera, ping, reboot, setting camera and watching event and alarm using onvif framework.

## Features:
1. Connect to a camera remotely
2. Ping camera remotely
3. Reboot camera remotely
4. Motion detection remotely
5. Setting camera (video decoder, fps, size,...)

# Setup Project

# How to command line

NVRControl ping --ip 10.96.34.218 -o 8899 -u admin -p admin123456
NVRControl reboot --ip 10.96.34.218 -o 8899 -u admin -p admin123456


# TODO
