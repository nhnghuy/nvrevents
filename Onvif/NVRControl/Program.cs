﻿using CommandLine;
using NVRControl.Device;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;

namespace NVRControl
{
    class Program
    {
        //const string deviceUri = new UriBuilder("http:/onvif/device_service");
        static int Main(string[] args)
        {
            return CommandLine.Parser.Default.ParseArguments<RebootOptions, PingOptions, ControlOptions>(args)
              .MapResult(
                (RebootOptions opts) => RunSystemRebootAndExit(opts),
                (PingOptions opts) => RunPingAndExit(opts),
                (ControlOptions opts) => RunSetFpsAndExit(opts),
                errs => 1);
        }

        private static DeviceClient Init(ICameraOptions options)
        {
            UriBuilder deviceUri = new UriBuilder("http:/onvif/device_service");
            deviceUri.Host = options.Ip;
            deviceUri.Port = options.Port;
            DeviceClient client = CreateClient<DeviceClient>(deviceUri.ToString(), options.Username, options.Password);
            if (!String.IsNullOrWhiteSpace(options.Username))
            {
                client.ClientCredentials.HttpDigest.ClientCredential.UserName = options.Username;
                client.ClientCredentials.HttpDigest.ClientCredential.Password = options.Password;
                client.ClientCredentials.HttpDigest.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            }
            return client;
        }

        private static bool IsConnected(DeviceClient client, out string error)
        {
            error = "";
            try
            {
                Service[] services = client.GetServices(false);
                if (services == null)
                {
                    error = "Cannot find any avaiable service. Onvif may turn-off or not avaiable.";
                    return false;
                }
            }
            catch (Exception ex)
            {
                error = "Can not connect to camera. Error:" + ex.Message;
                return false;
            }
            return true;
        }

        private static int RunSystemRebootAndExit(RebootOptions opts)
        {
            try
            {
                string error = "";
                DeviceClient client = Init(opts);

                if (IsConnected(client, out error))
                {
                    string s = client.SystemReboot();
                    Console.WriteLine("Reboot Success." + s);
                    return 0;
                } else
                {
                    Console.WriteLine("Error:" + error);
                    return -1;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Reboot Error:" + ex.Message);
                return -2;
            }
        }

        private static int RunPingAndExit(PingOptions opts)
        {
            try
            {
                string error = "";
                DeviceClient client = Init(opts);
                if (IsConnected(client, out error))
                {
                    Console.WriteLine("Ping Success.");
                    return 0;
                }
                else
                {
                    Console.WriteLine("Error:" + error);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Reboot Error:" + ex.Message);
            }
            return -1;
        }
        private static int RunSetFpsAndExit(ControlOptions opts)
        {
            try
            {
                string error = "";
                DeviceClient client = Init(opts);
                if (IsConnected(client, out error))
                {
                    //TODO Set Fps in video using onvif. Now do nothing.
                    return 0;
                }
                else
                {
                    Console.WriteLine("Error:" + error);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Reboot Error:" + ex.Message);
            }
            return -1;
        }

        public static T CreateClient<T>(string url, string username, string password)
        {
            HttpTransportBindingElement httpTransport = new HttpTransportBindingElement();
            httpTransport.AuthenticationScheme = System.Net.AuthenticationSchemes.Digest;
            System.ServiceModel.Channels.Binding binding = new CustomBinding(new TextMessageEncodingBindingElement(MessageVersion.Soap12WSAddressing10, Encoding.UTF8), httpTransport);
            return (T)Activator.CreateInstance(typeof(T), new object[] { binding, new EndpointAddress(url) });
        }
    }
}
