﻿using CommandLine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NVRControl
{
    interface ICameraOptions
    {
        [Option('i', "ip", Required = true, HelpText = "IP of camera that need to be connected.")]
        string Ip { get; set; }

        [Option('o', "port", Required = true, Default = 8899, 
            HelpText = "Port of OnVif Camera that need to be connected.")]
        int Port { get; set; }

        [Option('u', "username",
            HelpText = "User that use to access OnVif Camera.")]
        string Username { get; set; }

        [Option('p', "password",
            HelpText = "Password that use to access OnVif Camera.")]
        string Password { get; set; }
    }

    [Verb("reboot", HelpText = "Reboot the camera system.")]
    class RebootOptions : ICameraOptions
    {
        public string Ip { get; set; }
        public int Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }

    [Verb("ping", HelpText = "Ping Camera if they are still can be connected.")]
    class PingOptions : ICameraOptions
    {
        public string Ip { get; set; }
        public int Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }

    [Verb("setfps", HelpText = "Set Camera Controls.")]
    class ControlOptions : ICameraOptions
    {
        public string Ip { get; set; }
        public int Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        [Option("lusername",
            HelpText = "Local user that use to access Camera directly.")]
        public string LocalUsername { get; set; }

        [Option("lpassword",
            HelpText = "Local Password that use to access Camera directly.")]
        public string LocalPassword { get; set; }

        [Option("fps",
            HelpText = "Fps of Camera.")]
        public int Fps { get; set; }

    }
}