﻿using CommandLine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AlarmCSharpDemo;
using System.Globalization;
using System.Runtime.InteropServices;
using System.IO;
using Newtonsoft.Json;
using RestSharp;

namespace NVREvents
{
    class Program
    {
        private static Dictionary<string, Device> _devices = new Dictionary<string, Device>();
        private static CHCNetSDK.MSGCallBack_V31 _msgCbFunc = null;
        private static bool isSdkLoaded = false;
        private static string _portalAuthEndpoint;
        private static string _portalAuthUsername;
        private static string _portalAuthPassword;
        private static string _portalPushEndpoint;
        private static string _portalAuthToken;
        private static RestClient _portalRestClient;
        private static string _warehouseId;
        private static int _retryLoginInterval;

        static int Main(string[] args)
        {
            // Set program exit callback ___________________________
            AppDomain.CurrentDomain.ProcessExit += new EventHandler(CurrentDomain_ProcessExit);

            // Config CLI ________________________________________________
            return CommandLine.Parser.Default.ParseArguments<ArmOptions>(args)
              .MapResult(
                (ArmOptions opts) => RunArm(opts),
                errs => 1);
        }

        private static string GetAuthToken(string username, string password)
        {
            var request = new RestRequest(_portalAuthEndpoint, Method.POST, DataFormat.Json);
            request.AddJsonBody(new { identifier = username, password = password });

            IRestResponse response = _portalRestClient.Execute(request);
            var responseBody = JsonConvert.DeserializeObject<Dictionary<string, object>>(response.Content);

            if (response.ErrorException != null)
            {
                Console.WriteLine($"Get auth token failed. Cause : {response.ErrorException.Message}");
                return null;
            }
            if (response.StatusCode != HttpStatusCode.OK)
            {
                object errSummary, errDetail;
                responseBody.TryGetValue("error", out errSummary);
                responseBody.TryGetValue("message", out errDetail);
                Console.WriteLine($"Get auth token failed. Cause : {errSummary} - {errDetail}");
                return null;
            }
            if (!String.IsNullOrWhiteSpace(response.ErrorMessage))
            {
                Console.WriteLine($"Get auth token failed. Cause : {response.ErrorMessage}");
                return null;
            }

            _portalAuthToken = responseBody["jwt"] != null ? responseBody["jwt"].ToString() : null;
            return _portalAuthToken;
        }

        private static async Task<bool> PushToPortal(Object deviceEvent)
        {
            if (String.IsNullOrWhiteSpace(_portalAuthToken))
            {
                _portalAuthToken = GetAuthToken(_portalAuthUsername, _portalAuthPassword);
            }

            var request = new RestRequest(_portalPushEndpoint, Method.POST, DataFormat.Json);
            request.AddHeader("Authorization", "Bearer " + _portalAuthToken);
            request.AddJsonBody(JsonConvert.SerializeObject(deviceEvent));

            IRestResponse response = _portalRestClient.Execute(request);
            if (response.ErrorException != null)
            {
                Console.WriteLine($"Push failed. Cause : {response.ErrorException.Message}");
                return false;
            }
            else if (response.StatusCode != HttpStatusCode.OK)
            {
                var responseBody = JsonConvert.DeserializeObject<Dictionary<string, object>>(response.Content);
                object errSummary, errDetail;
                responseBody.TryGetValue("error", out errSummary);
                responseBody.TryGetValue("message", out errDetail);
                Console.WriteLine($"Push failed. Cause : {errSummary} - {errDetail}");
                return false;
            }
            return true;
        }

        static void CurrentDomain_ProcessExit(object sender, EventArgs e)
        {
            Console.WriteLine("Prepare to exit...");

            List<Task> logoutTasks = new List<Task>();
            foreach (var entry in _devices)
            {
                logoutTasks.Add(LogoutDevice(entry.Value));
            }
            Task.WaitAll(logoutTasks.ToArray());

            if (isSdkLoaded) CHCNetSDK.NET_DVR_Cleanup();
        }

        private static async Task LogoutDevice(Device device)
        {
            // Disarm ___________________________________________________________
            if (device.SdkUserID < 0 || device.SdkAlarmHandle < 0) return;
            var msg = $"Disarm {device.Host}";
            if (!CHCNetSDK.NET_DVR_CloseAlarmChan_V30(device.SdkAlarmHandle))
                Console.WriteLine($"{msg} failed! DVR Error Code {CHCNetSDK.NET_DVR_GetLastError()}");
            else
                Console.WriteLine($"{msg} successfully!");

            // Logout ___________________________________________________________
            if (device.SdkUserID < 0) return;
            msg = $"Logout from {device.Host}";
            if (!CHCNetSDK.NET_DVR_Logout(device.SdkUserID))
                Console.WriteLine($"{msg} failed! DVR Error Code {CHCNetSDK.NET_DVR_GetLastError()}");
            else
                Console.WriteLine($"{msg} successfully!");
        }

        private static bool MsgCallback_V31(int lCommand, ref CHCNetSDK.NET_DVR_ALARMER pAlarmer, IntPtr pAlarmInfo, uint dwBufLen, IntPtr pUser)
        {
            // filter COMM_ALARM_V30
            if (lCommand != CHCNetSDK.COMM_ALARM_V30) return true;

            // struct
            CHCNetSDK.NET_DVR_ALARMINFO_V30 alarmInfoV30 = new CHCNetSDK.NET_DVR_ALARMINFO_V30();
            alarmInfoV30 = (CHCNetSDK.NET_DVR_ALARMINFO_V30)Marshal.PtrToStructure(pAlarmInfo, typeof(CHCNetSDK.NET_DVR_ALARMINFO_V30));

            if (alarmInfoV30.dwAlarmType == 3)
            {
                var device = _devices[pAlarmer.sDeviceIP];
                int channel = 0;
                for (int i = 0; i < CHCNetSDK.MAX_CHANNUM_V30; i++)
                {
                    if (alarmInfoV30.byChannel[i] == 1)
                    {
                        channel = (i + 1);
                    }
                }
                var message = $"Motion detection on channel {channel} from camera {device.Id} ({device.Host}) in warehouse {_warehouseId}";
                var deviceEvent = new DeviceEvent(_warehouseId, device.Id, message);
                Console.WriteLine($"{message}, at {deviceEvent.Timestamp.ToString("yyyy-MM-dd'T'HH:mm:ss.fffK", CultureInfo.InvariantCulture)}");
                PushToPortal(deviceEvent);
            }

            return true;
        }

        private static Boolean ProcessCliArgs(ICameraOptions opts)
        {
            _warehouseId = opts.WarehouseId;
            _portalRestClient = new RestClient(opts.PortalBaseUrl);
            _portalAuthEndpoint = opts.PortalAuthEndpoint;
            _portalAuthUsername = opts.PortalUsername;
            _portalAuthPassword = opts.PortalPassword;
            _portalPushEndpoint = opts.PortalPushEndpoint;
            _retryLoginInterval = opts.RetryLoginIntervalInSeconds * 1000;

            if (!String.IsNullOrWhiteSpace(opts.JsonString))
            {
                _devices = JsonConvert.DeserializeObject<HashSet<Device>>(opts.JsonString).ToDictionary(e => e.Host);
                return true;
            }

            if (!String.IsNullOrWhiteSpace(opts.TextFile))
            {
                ParseTextFileToDevices(opts.TextFile);
                return true;
            }

            if (String.IsNullOrWhiteSpace(opts.Host))
            {
                Console.WriteLine("Error : Device IP is required");
                return false;
            }

            _devices.Add(opts.Host, new Device(opts.Id, opts.Host, opts.Port, opts.Username, opts.Password));
            return true;
        }

        private static int RunArm(ArmOptions opts)
        {
            if (!ProcessCliArgs(opts)) {
                return -1;
            }

            // Init CHCNetSDK _____________________________________________________
            isSdkLoaded = CHCNetSDK.NET_DVR_Init();
            if (isSdkLoaded == false)
            {
                Console.WriteLine("Init CHCNetSDK failed!");
                return -1;
            }

            // Set CHCNetSDK message callback ___________________________
            if (_msgCbFunc == null)
            {
                _msgCbFunc = new CHCNetSDK.MSGCallBack_V31(MsgCallback_V31);
            }
            CHCNetSDK.NET_DVR_SetDVRMessageCallBack_V31(_msgCbFunc, IntPtr.Zero);

            // Arm device(s) __________________________________________________
            CHCNetSDK.NET_DVR_SETUPALARM_PARAM alarmParam = GenerateAlarmParam();
            foreach (var entry in _devices)
            {
                ArmAsync(entry.Value, alarmParam);
            }

            Console.WriteLine("Press [ENTER] to terminate...");
            Console.Read();
            return 0;
        }

        private static async Task ArmAsync(Device device, CHCNetSDK.NET_DVR_SETUPALARM_PARAM alarmParam)
        {
            // Login the device(s) __________________________________________________
            CHCNetSDK.NET_DVR_DEVICEINFO_V30 DeviceInfo = new CHCNetSDK.NET_DVR_DEVICEINFO_V30();
            var msg = $"Login to {device.Host}";
            while (true)
            {
                device.SdkUserID = CHCNetSDK.NET_DVR_Login_V30(device.Host, device.Port, device.Username, device.Password, ref DeviceInfo);
                if (device.SdkUserID < 0)
                {
                    Console.WriteLine($"{msg} failed! DVR Error Code {CHCNetSDK.NET_DVR_GetLastError()}. Retry login in {(float)_retryLoginInterval/1000} second(s)");
                    await Task.Delay(_retryLoginInterval);
                }
                else
                {
                    Console.WriteLine($"{msg} successfully! SdkUserID : {device.SdkUserID}");
                    break;
                }
            }

            // Arm the device(s) ______________________________________________
            msg = $"Arm {device.Id} ({device.Host})";
            while (true)
            {
                device.SdkAlarmHandle = CHCNetSDK.NET_DVR_SetupAlarmChan_V41(device.SdkUserID, ref alarmParam);
                if (device.SdkUserID < 0)
                {
                    Console.WriteLine($"{msg} failed! DVR Error Code {CHCNetSDK.NET_DVR_GetLastError()}. Retry arm in {(float)_retryLoginInterval / 1000} second(s)");
                    await Task.Delay(_retryLoginInterval);
                }
                else
                {
                    Console.WriteLine($"{msg} successfully! SdkAlarmHandle : {device.SdkAlarmHandle}");
                    break;
                }
            }
        }

        private static CHCNetSDK.NET_DVR_SETUPALARM_PARAM GenerateAlarmParam()
        {
            CHCNetSDK.NET_DVR_SETUPALARM_PARAM alarmParam = new CHCNetSDK.NET_DVR_SETUPALARM_PARAM();
            alarmParam.dwSize = (uint)Marshal.SizeOf(alarmParam);
            alarmParam.byLevel = 1;
            alarmParam.byAlarmInfoType = 1;
            alarmParam.byFaceAlarmDetection = 1;
            return alarmParam;
        }

        private static bool ParseTextFileToDevices(string path)
        {
            var lines = File.ReadLines(@path);
            foreach (var line in lines)
            {
                var info = line.Split(new string[] { " ", "\t" }, StringSplitOptions.RemoveEmptyEntries);
                _devices.Add(info[1], new Device(info[0], info[1], Int32.Parse(info[2]), info[3], info[4]));
            }
            return true;
        }

    }

}

