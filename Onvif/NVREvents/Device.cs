﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NVREvents
{
    class Device : IEquatable<Device>
    {
        public Device(string id, string host, int port, string username, string password)
        {
            Id = id;
            Host = host;
            Port = port;
            Username = username;
            Password = password;
        }

        public string Id { get; set; }

        public string Host { get; set; }

        public int Port { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public int SdkUserID { get; set; } = -1;

        public int SdkAlarmHandle { get; set; } = -1;

        public override string ToString()
        {
            return $"Device - ID: {Id} | IP: {Host} | Port: {Port} | Username: {Username} | Password: {Password}";
        }

        public bool Equals(Device other)
        {
            return this.Host.Equals(other.Host);
        }

        public override bool Equals(object obj)
        {
            return obj is Device ? Equals((Device) obj) : false;
        }

        public override int GetHashCode()
        {
            return this.Host.GetHashCode();
        }
    }
}
