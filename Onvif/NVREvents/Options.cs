﻿using CommandLine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NVREvents
{
    interface ICameraOptions
    {
        [Option("cameras-json", HelpText = "Input cameras as json string.")]
        string JsonString { get; set; }

        [Option("cameras-file", HelpText = "Input cameras as text file.")]
        string TextFile { get; set; }

        [Option("wid", HelpText = "Warehouse's ID.")]
        string WarehouseId { get; set; }

        [Option("id", HelpText = "Camera's ID.")]
        string Id { get; set; }

        [Option('h', "host", HelpText = "IP of camera that need to be connected.")]
        string Host { get; set; }

        [Option('o', "port", Default = 8000, HelpText = "Port of HikVision Camera that need to be connected.")]
        int Port { get; set; }

        [Option('u', "username", HelpText = "Username that use to access HikVision Camera.")]
        string Username { get; set; }

        [Option('p', "password", HelpText = "Password that use to access HikVision Camera.")]
        string Password { get; set; }

        [Option("portal-base-url", Default = "http://localhost:1337", HelpText = "Portal's base url.")]
        string PortalBaseUrl { get; set; }

        [Option("portal-auth-endpoint", Default = "auth/local", HelpText = "The endpoint for authentication")]
        string PortalAuthEndpoint { get; set; }

        //[Option("portal-host", Default = "localhost", HelpText = "Portal's address.")]
        //string PortalHost { get; set; }

        //[Option("portal-port", Default = 1337, HelpText = "Portal's port.")]
        //int PortalPort { get; set; }

        [Option("portal-username", Default = "admin", HelpText = "Username that use to authenticate with portal.")]
        string PortalUsername { get; set; }

        [Option("portal-password", Default = "123456", HelpText = "Password that use to authenticate with portal.")]
        string PortalPassword { get; set; }

        [Option("portal-push-endpoint", Default = "alarms", HelpText = "The endpoint to push device events")]
        string PortalPushEndpoint { get; set; }

        [Option("retry-login-cam-interval", Default = "60", HelpText = "The interval in seconds between retry login to camera")]
        int RetryLoginIntervalInSeconds { get; set; }
    }

    [Verb("arm", HelpText = "Arms camera if they are still can be connected.")]
    class ArmOptions : ICameraOptions
    {
        public string JsonString { get; set; }
        public string TextFile { get; set; }
        public string WarehouseId { get; set; }
        public string Id { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string PortalBaseUrl { get; set; }
        public string PortalAuthEndpoint { get; set; }
        //public string PortalHost { get; set; }
        //public int PortalPort { get; set; }
        public string PortalUsername { get; set; }
        public string PortalPassword { get; set; }
        public string PortalPushEndpoint { get; set; }
        public int RetryLoginIntervalInSeconds { get; set; }
    }

}