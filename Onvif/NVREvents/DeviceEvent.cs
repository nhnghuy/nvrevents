﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace NVREvents
{
    public class DeviceEvent
    {
        [JsonProperty("warehouseId")]
        public string WarehouseId { get; }

        [JsonProperty("cameraId")]
        public string DeviceId { get; }

        [JsonProperty("alarmAt")]
        public DateTime Timestamp { get; } = DateTime.UtcNow;

        [JsonProperty("alarmMessage")]
        public string Message { get; }

        public DeviceEvent(string warehouseId, string deviceId, string message)
        {
            WarehouseId = warehouseId;
            DeviceId = deviceId;
            Message = message;
        }

        public override string ToString()
        {
            return $"{WarehouseId} - {DeviceId} - {Message}, at {Timestamp.ToString("yyyy-MM-dd'T'HH:mm:ss.fffK", CultureInfo.InvariantCulture)}";
        }
    }

}
